const nodemailer = require("nodemailer");
const ejs = require("ejs");
const path = require("path");
const moment = require("moment");
const pool = require("../config/dbconfig");
const config = require("../config/nodemalerConfig");
const log = require("../logger/logger");
const updateEmailStatus = require("../model/emailQueue").updateEmailStatus;
const selectPendingEmails = require("../model/emailQueue").selectPendingEmails;
const transporter = nodemailer.createTransport(config.email);

async function sendMail() {
  try {
    const now = moment().format("YYYY-MM-DD HH:mm:ss");

    //Select emails that are ready to be sent
    const rows = await selectPendingEmails(now);
    log.info(`List of pending status rows: ${JSON.stringify(rows)}`);
    for (const row of rows) {
      let { id, email, bcc, subject, body_data } = row;

      //Template creation
      const templatePath = path.join(__dirname, "..", "views", "index.html");
      const html = await ejs.renderFile(templatePath, body_data);

      // Update status to 'processing'
      await updateEmailStatus("processing", id);

      //Sending mail
      const mailOptions = {
        from: config.mailOption,
        to: email.join(", "),
        bcc: bcc.join(", "),
        subject: subject,
        html: html,
      };
      transporter.sendMail(mailOptions, async function (err, data) {
        if (err) {
          log.error(`Failed to send email ID ${id}: ${err.message}`);
          await updateEmailStatus("failed", id);
        } else {
          log.info(`Email sent successfully for ID ${id}`);
          await updateEmailStatus("completed", id);
        }
      });
      log.info(`Queue is updates successfully with correct status`);
    }
  } catch (error) {
    log.error(`Updation Fail ${error}`);
    await updateEmailStatus("failed", id);
  }
}

module.exports = sendMail;
