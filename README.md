Email Notification Service
This service provides a REST API endpoint for sending notification emails to specified email addresses. The emails can be sent immediately or scheduled for later delivery.

Requirements
Node.js
PostgreSQL

Installation

Clone the repository:
git clone https://github.com/your-repo/email-notification-service.git
cd email-notification-service

Prerequisites
Template Creation
Before Running services create html template by running app.js which convert task-icewarp.eml into index.html

Install dependencies:
npm i

Setup environment variables:
Create a .env file in the root directory and add the following environment variables .Replace it with your credential:
PG_HOST=localhost
PG_USER=your_postgres_username
PG_PASS=your_postgres_password
PG_PORT=5432
PG_DB=your_postgres_dbname

API Endpoint
POST /email

Request Body:

 {
 "key": "task-icewarp",
 "delayed_send": "2024-05-22 15:30:00",
 "email": [
 "tonda@icewarp.com"
 ],
 "bcc": [
 "info@icewarp.com"
 ],
 "body_data": {
 "name": "Tonda",
 "days": "7",
 "link": {
 "label": "click here",
 "url": "https://www.icewarp.com"
 }
 }
 }

Response:

200 Accepted if the Email sent Sucessfully
500 Internal Server Error if there is an error

