const fs = require('fs');
const emlformat = require('eml-format');
const { Module } = require('module');

function convertEmlToHtml(emlPath, htmlPath) {
    // Read the eml file
    fs.readFile(emlPath, 'utf-8', (err, data) => {
        if (err) {
            console.error('Error reading the EML file:', err);
            return;
        }

        // Parse the eml file
        emlformat.read(data, (error, eml) => {
            if (error) {
                console.error('Error parsing the EML file:', error);
                return;
            }

            // Extract the HTML content from the parsed EML
            const htmlContent = eml.html || eml.text;

            // Write the HTML content to an HTML file
            fs.writeFile(htmlPath, htmlContent, (writeErr) => {
                if (writeErr) {
                    console.error('Error writing the HTML file:', writeErr);
                    return;
                }

                console.log('HTML file created successfully!');
            });
        });
    });
}

const emlPath = './views/task-icewarp.eml';
const htmlPath = './views/index.html';
convertEmlToHtml(emlPath, htmlPath);


