require("dotenv").config();
const express = require("express");
const app = express();
const router = require("./router/routerIndex");
const port = process.env.PORT;

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(router);

app.listen(port, () => {
  console.log("server is up on port " + port);
});
