const fs = require('fs');
const path = require('path');

const logFilePath = path.join(__dirname, 'app.log');

function log(level, message) {
    const logMessage = `${new Date().toISOString()} ${level}: ${message}\n`;
    console.log(logMessage.trim());
    fs.appendFileSync(logFilePath, logMessage);
}

module.exports = {
    info: (message) => log('INFO', message),
    error: (message) => log('ERROR', message),
};
