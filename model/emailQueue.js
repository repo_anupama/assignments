const pool = require("../config/dbconfig");
const log = require("../logger/logger");
const moment = require("moment");

async function createEmailQueueTable() {
  try {
    const createpartnertable = `
            CREATE TABLE IF NOT EXISTS email_queue (
                id SERIAL PRIMARY KEY,
                key TEXT NOT NULL,
                subject TEXT,
                delayed_send TIMESTAMP WITH TIME ZONE,
                email TEXT[],
                bcc TEXT[],
                body_data JSONB,
                status TEXT DEFAULT 'pending',
                created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
            );
        `;

    const response = await pool.query(createpartnertable);
    log.info(`Email Queue table created:${response}`);
  } catch (error) {
    log.error(`Error creating email_queue table: ${error.message}`);
  }
}

async function insertEmailQueueTable(data) {
  try {
    let subject = "email subject";
    log.info(
      `Received email request for key: ${data.key}, subject: ${subject}`
    );

    const { key, delayed_send, body_data, email, bcc } = data;
    let delayedSendTimestamp = null;
    if (delayed_send) {
      delayedSendTimestamp = moment.utc(delayed_send).format();
    }

    const query = `
            INSERT INTO email_queue (key, subject, delayed_send, email, bcc, body_data)
            VALUES ($1, $2, $3, $4, $5, $6)
        `;
    const values = [key, subject, delayedSendTimestamp, email, bcc, body_data];
    const response = await pool.query(query, values);

    log.info(
      `Email inserted in to Queue ${JSON.stringify(response.rowCount)} `
    );
  } catch (error) {
    log.error(`Error in inserting email_queue table: ${error.message}`);
  }
}
async function selectPendingEmails(now) {
  try {
    const query = `
            SELECT * FROM email_queue
            WHERE status = 'pending'
            AND (delayed_send IS NULL OR delayed_send <= $1)
            LIMIT 10
        `;
    const values = [now];
    const { rows } = await pool.query(query, values);
    log.info(`Selected pending emails: ${JSON.stringify(rows)}`);
    return rows;
  } catch (error) {
    log.error(`Failed to select pending emails: ${error.message}`);
    throw error;
  }
}
async function updateEmailStatus(status, id) {
  try {
    const query = "UPDATE email_queue SET status = $1 WHERE id = $2";
    const values = [status, id];
    await pool.query(query, values);
    log.info(`Updated email ID ${id} to status '${status}'`);
  } catch (error) {
    log.error(
      `Failed to update email ID ${id} to status '${status}': ${error.message}`
    );
    throw error;
  }
}
module.exports = {
  createEmailQueueTable: createEmailQueueTable,
  insertEmailQueueTable: insertEmailQueueTable,
  updateEmailStatus: updateEmailStatus,
  selectPendingEmails: selectPendingEmails,
};
