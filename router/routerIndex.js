const router = require("express").Router();
const sendMail = require("../services/mailServices");
const createEmailQueueTable =
  require("../model/emailQueue").createEmailQueueTable;
const insertEmailQueueTable =
  require("../model/emailQueue").insertEmailQueueTable;
const moment = require("moment");
const log = require("../logger/logger");
const pool = require("../config/dbconfig");

router.post("/email", async (req, res) => {
  try {
    log.info(`Process Started`);

    // Create the email queue table if it doesn't exist
    await createEmailQueueTable();
    log.info(`Email queue table created`);

    // Insert the email request into the email queue table
    await insertEmailQueueTable(req.body);
    log.info(`Email request inserted into queue`);

    // Process the email sending
    await sendMail();
    log.info(`Email sent successfully`);

    // Respond with success message
  } catch (error) {
    log.error(`Failed while sending message :${error}`);
    res.status(500).json({ message: "Failed while sending message" });
  }
});

module.exports = router;
